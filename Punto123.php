<?php

// Fecha: 17 de enero de 2021
// Desarrollador: Ana del Carmen Carolina Angel Cepeda

//Variables punto 1
$variable1=5;
$variable2=7;

//Varibles punto 2
$n=15;

//Variables punto 3

$citiesList = [
 "Zipaquirá",
 "Bogotá",
 "Cali",
 "Medellín",
 "Barranquilla",
 "Cartagena",
 "Bogotá",
 "Zipaquirá",
 "Zipaquirá",
 "Cali",
 "Barranquilla",
 "Medellín",
 "Bogotá",
 "Cartagena",
 "Medellín",
 "Bogotá",
 "Cali",
 "Zipaquirá",
 "Girardot",
 "Zipaquirá",
 "Cali",
 "Zipaquirá",
 "Zipaquirá",
 "Cali",
 "Bogotá",
 "Medellín",
 "Cali",
 "Barranquilla",
];
$cantCity=3;

echo "PUNTO 1 <br />";
echo addTwoNumbers($variable1, $variable2);

echo "PUNTO 2 <br />";
echo impares3y5($n);

echo "<br /><br />PUNTO 3 <br />";
$finCiudad= ordenCiudad($cantCity, $citiesList);
foreach ($finCiudad as $key => $value) { echo $key." - ".$value." veces <br />"; }


function addTwoNumbers($variable1, $variable2){
	$suma=$variable1+$variable2;
	return 'La suma entre '.$variable1.' y '.$variable2.' es: '.$suma."<br /><br />";
}

function impares3y5($n){

	$cadena= '';

	if($n==0){ return '0'; }
	else{ 
		for($i=0; $i<=$n; $i++){
			$on=false;
			if($i==0){ $cadena .= '0'; $on=true;}
			
			if($i>2){
				$div3 = $i % 3;
				$div5 = $i % 5;

				if($div3==0 && $div5==0){ $cadena .= ', fizzbuzz'; $on=true;}
				else{
					if($div3==0){ $cadena .= ', fizz'; $on=true;}
					else{ if($div5==0){	$cadena .= ', buzz'; $on=true;}
					}
				}
			}
			if(!$on){$cadena .= ','.$i;}
		}
	}
	return $cadena;
}

function ordenCiudad($cantCity, $citiesList){

	$contarCiudad= array_count_values($citiesList);	
	arsort($contarCiudad);
	
	if($cantCity>0 || $cantCity< count($contarCiudad)){ $final = array_slice($contarCiudad, 0, $cantCity);}
		else{return "El número debe ser mayor a cero o no puede superar el límite";};
	return $final;
}


?>
