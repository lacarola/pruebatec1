

select concat(u.first_name," ", u.last_name) as Usuario, p.name as Playlist, s.name  as Canción, s.gender as Genero, a.name as Artista from users u 
join playlists p on p.user_id =u.id
join songs s on s.id =p.song_id 
join artists a on a.id = s.artist_id;


select a.name as Artista, a2.name as Albumes, a2.release_date as Fecha_de_Lanzamiento, s.gender as Genero from artists a
join songs s on s.artist_id =a.id  
join albums a2 on a2.artist_id = a.id ;

select a.name as Artista, p2.id as podcasts, p2.duration as duración ,p2.gender as Genero from artists a
join podcasts p2 on p2.artist_id =a.id ;

select a.name as Artista, a2.name as Albumes, a2.release_date as Fecha_de_Lanzamiento, s.gender as Genero from artists a
join songs s on s.artist_id =a.id  
join albums a2 on a2.artist_id = a.id 
where a.bitactivo =0;